from rest_framework import serializers

from besTenderApp.models import Transaccion


class TransaccionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaccion
        fields = ['idTransaccion', 'idInventario', 'idUser', 'fechaTransaccion', 'tipoTransaccion','precioTotal']
