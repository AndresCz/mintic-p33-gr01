from besTenderApp.models.inventario import Inventario
from rest_framework import serializers


class InventarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Inventario
        fields = ['idInventario', 'idProducto', 'idUser', 'cantidad']

    def to_representation(self, obj):
        inventario = Inventario.objects.get(id=obj.idInventario)

        return {
            "idInventario": inventario.idInventario,
            "idProducto": inventario.idProducto,
            "idUser": inventario.idUser,
            "cantidad": inventario.cantidad
        }
