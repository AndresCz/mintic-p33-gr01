from besTenderApp.models import Proveedor
from besTenderApp.models.producto import Producto
from rest_framework import serializers


class productoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Producto
        fields = ['idProducto','nombreProducto', 'idProveedor','idTransaccion','categoriaProducto', 'descripcion', 'precio', 'fechaVencimiento']
# No va todos los campos por motivos de seguridad (es lo que ve el usuario) la llave foranea automaticamenta django la gestiona

    def to_representation(self, obj):
        producto = Producto.objects.get(id=obj.idProducto)
        proveedor = Proveedor.objects.get(producto=obj.idProveedor) ## aun no se como colocar el objeto proveedor

        return {
            "idProducto": producto.idProducto,
            "nombreProducto": producto.nombreProducto,
            "idProveedor": producto.idProveedor, ##No se como traer el proveedor completo
            "idTransaccion": producto.idTransaccion,
            "categoriaProducto": producto.categoriaProducto,
            "descripcion": producto.descripcion,
            "precio": producto.precio,
            "fechaVencimiento": producto.fechaVencimiento
        }