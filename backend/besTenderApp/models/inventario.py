from django.db import models
from besTenderApp.models.producto import Producto
from besTenderApp.models.user import User


class Inventario (models.Model):
    idInventario = models.AutoField(primary_key=True)
    idProducto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    idUser = models.ForeignKey(User, on_delete=models.CASCADE)
    cantidad = models.CharField(max_length=100, blank=False, null=False)
