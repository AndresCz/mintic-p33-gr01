from django.contrib import admin

# Register your models here.
from besTenderApp.models import User, Inventario, Transaccion, Producto, Proveedor

admin.site.register(User)
admin.site.register(Inventario)
admin.site.register(Proveedor)
admin.site.register(Transaccion)
admin.site.register(Producto)
